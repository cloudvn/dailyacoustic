package com.hazuu.dailyacoustic;

import de.greenrobot.daogenerator.DaoGenerator;
import de.greenrobot.daogenerator.Entity;
import de.greenrobot.daogenerator.Schema;

public class AcousticDaoGenerator {

    public static void main(String args[]) throws Exception {
        Schema schema = new Schema(1, "com.hazuu.dailyacoustic.models");
        Entity box = schema.addEntity("Radio");
        box.addIdProperty();
        box.addStringProperty("title");
        box.addStringProperty("image");
        box.addStringProperty("audio");
        new DaoGenerator().generateAll(schema, args[0]);
    }
}
