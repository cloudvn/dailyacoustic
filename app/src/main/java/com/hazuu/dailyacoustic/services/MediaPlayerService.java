package com.hazuu.dailyacoustic.services;

import android.app.Notification;
import android.app.NotificationManager;
import android.app.PendingIntent;
import android.app.Service;
import android.content.Intent;
import android.graphics.Bitmap;
import android.media.MediaPlayer;
import android.os.Build;
import android.os.Handler;
import android.os.IBinder;
import android.support.v4.app.NotificationCompat;
import android.widget.RemoteViews;

import com.hazuu.dailyacoustic.R;
import com.hazuu.dailyacoustic.events.PlayerErrorEvent;
import com.hazuu.dailyacoustic.events.PlayerStateUpdateEvent;
import com.hazuu.dailyacoustic.events.RadioPlayerActionEvents;
import com.hazuu.dailyacoustic.events.UpdateBufferingEvent;
import com.hazuu.dailyacoustic.events.UpdatedCurrentRadioEvent;
import com.hazuu.dailyacoustic.helper.Constants;
import com.hazuu.dailyacoustic.helper.PlayerStateHandler;
import com.hazuu.dailyacoustic.models.Radio;
import com.hazuu.dailyacoustic.receivers.NotificationActionsReceiver;
import com.hazuu.dailyacoustic.ui.MainActivity;
import com.squareup.picasso.Picasso;

import java.io.IOException;

import de.greenrobot.event.EventBus;

/**
 * Created by donnguyen on 6/22/14.
 */
public class MediaPlayerService extends Service implements MediaPlayer.OnCompletionListener, MediaPlayer.OnBufferingUpdateListener, MediaPlayer.OnErrorListener {

    private MediaPlayer mediaPlayer = null;

    private static int classID = 567; // just a number

    public static String START_PLAY = "START_PLAY";

    private final Handler handler = new Handler();

    private int fileDurationInMilliseconds;

    private Radio currentRadio = null;

    Runnable notificationRunnable;

    @Override
    public IBinder onBind(Intent intent) {
        return null;
    }

    @Override
    public void onCreate() {
        super.onCreate();
        mediaPlayer = new MediaPlayer();
        mediaPlayer.setOnBufferingUpdateListener(this);
        mediaPlayer.setOnCompletionListener(this);
        EventBus.getDefault().registerSticky(this);
    }

    @Override
    public int onStartCommand(Intent intent, int flags, int startId) {
        UpdatedCurrentRadioEvent updatedCurrentRadioEvent = EventBus.getDefault().getStickyEvent(UpdatedCurrentRadioEvent.class);
        if(updatedCurrentRadioEvent != null){
            currentRadio = updatedCurrentRadioEvent.getCurrentRadio();
        }

        return Service.START_STICKY;
    }

    public void onEventAsync(RadioPlayerActionEvents.PlayEvent playEvent) {
        currentRadio = playEvent.getRadio();
        play(currentRadio.getAudio());
    }

    public void onEventAsync(RadioPlayerActionEvents.PauseOrResumeEvent pauseOrResumeEvent) {
        pauseOrResume();
    }

    private void pauseOrResume() {
        if (PlayerStateHandler.getInstance().isPlaying()) {
            PlayerStateHandler.getInstance().setState(Constants.PLAYER_STATE_IS_PAUSE);
            buildNotification();
            mediaPlayer.pause();
        }else{
            PlayerStateHandler.getInstance().setState(Constants.PLAYER_STATE_IS_PLAYING);
            buildNotification();
            mediaPlayer.start();
        }
    }

    public void onEventAsync(RadioPlayerActionEvents.SeekToEvent seekToEvent) {
        seekTo(seekToEvent);
    }

    private void seekTo(RadioPlayerActionEvents.SeekToEvent seekToEvent) {
        mediaPlayer.seekTo(seekToEvent.getPosition());
    }

    public void onEventAsync(RadioPlayerActionEvents.NextEvent nextEvent) {
        currentRadio = nextEvent.getRadio();
        play(currentRadio.getAudio());
    }

    public void onEventAsync(RadioPlayerActionEvents.PrevEvent prevEvent) {
        currentRadio = prevEvent.getRadio();
        play(currentRadio.getAudio());
    }

    public void onEventAsync(RadioPlayerActionEvents.StopEvent stopEvent) {
        handler.removeCallbacks(notificationRunnable);
        PlayerStateHandler.getInstance().setState(Constants.PLAYER_STATE_IS_STOP);
        this.stopSelf();
    }

    private void play(String radioUrl) {
        try {
            PlayerStateHandler.getInstance().setState(Constants.PLAYER_STATE_IS_PLAYING);
            buildNotification();

            mediaPlayer.reset();
            mediaPlayer.setDataSource(radioUrl);
            mediaPlayer.prepare();
            fileDurationInMilliseconds = mediaPlayer.getDuration();
            mediaPlayer.setLooping(true); // this will make it loop forever
            mediaPlayer.start();
            EventBus.getDefault().postSticky(new UpdatedCurrentRadioEvent(currentRadio));

            PlayerStateUpdater();
        } catch (IOException e) {
            PlayerStateHandler.getInstance().setState(Constants.PLAYER_STATE_IS_STOP);
            EventBus.getDefault().post(new PlayerErrorEvent());
        }
    }

    private void PlayerStateUpdater() {
        if(mediaPlayer != null && currentRadio != null){
            boolean isPlaying = PlayerStateHandler.getInstance().isPlaying();
            EventBus.getDefault().postSticky(new PlayerStateUpdateEvent(mediaPlayer.getCurrentPosition(), fileDurationInMilliseconds, isPlaying, currentRadio));

            notificationRunnable = new Runnable() {
                public void run() {
                    PlayerStateUpdater();
                }
            };
            handler.postDelayed(notificationRunnable, 1000);
        }
    }

    @Override
    public void onDestroy() {
        stop();
        EventBus.getDefault().unregister(this);
    }

    private void stop() {
        if (mediaPlayer != null) {
            mediaPlayer.release();
            mediaPlayer = null;
            PlayerStateHandler.getInstance().setState(Constants.PLAYER_STATE_IS_STOP);
        }
    }

    @Override
    public void onBufferingUpdate(MediaPlayer mediaPlayer, int percent) {
        EventBus.getDefault().post(new UpdateBufferingEvent(percent));
    }

    @Override
    public void onCompletion(MediaPlayer mediaPlayer) {

    }

    @Override
    public boolean onError(MediaPlayer mediaPlayer, int what, int extra) {
        EventBus.getDefault().post(new PlayerErrorEvent());
        PlayerStateHandler.getInstance().setState(Constants.PLAYER_STATE_IS_STOP);
        buildNotification();
        return false;
    }

    private void buildNotification() {
        Intent intent = new Intent(this, MainActivity.class);
        intent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP |
                Intent.FLAG_ACTIVITY_SINGLE_TOP);

        PendingIntent pi = PendingIntent.getActivity(this, 0, intent, 0);

        NotificationCompat.Builder notificationBuilder = new NotificationCompat.Builder(getApplicationContext())
                .setContentTitle(getString(R.string.app_name))
                .setContentText(currentRadio.getTitle())
                .setSmallIcon(R.drawable.ic_launcher)
                .setContentIntent(pi);

        NotificationManager mNotifyMgr =
                (NotificationManager) getSystemService(NOTIFICATION_SERVICE);
        Notification notification = notificationBuilder.build();
        notification.flags |= Notification.FLAG_NO_CLEAR;
        notification.flags |= Notification.FLAG_ONGOING_EVENT;

        if(Build.VERSION.SDK_INT >= 16){
            RemoteViews remoteView = new RemoteViews(this.getPackageName(), R.layout.playing_notification);

            remoteView.setTextViewText(R.id.app_name, getString(R.string.app_name));
            remoteView.setTextViewText(R.id.current_radio, currentRadio.getTitle());

            Intent resumeOrPauseIntent = new Intent(this, NotificationActionsReceiver.class);
            resumeOrPauseIntent.putExtra(Constants.PLAYER_ACTION_FROM_NOTIFICATION, Constants.PLAYER_RESUME_OR_PLAY);
            PendingIntent pendingResumeOrPauseIntent = PendingIntent.getBroadcast(this, 0, resumeOrPauseIntent, PendingIntent.FLAG_UPDATE_CURRENT);
            remoteView.setOnClickPendingIntent(R.id.btnPlayPause, pendingResumeOrPauseIntent);

            Intent nextIntent = new Intent(this, NotificationActionsReceiver.class);
            nextIntent.putExtra(Constants.PLAYER_ACTION_FROM_NOTIFICATION, Constants.PLAYER_NEXT);
            PendingIntent pendingNextIntent = PendingIntent.getBroadcast(this, 1, nextIntent, PendingIntent.FLAG_UPDATE_CURRENT);
            remoteView.setOnClickPendingIntent(R.id.btnNext, pendingNextIntent);

            Intent prevIntent = new Intent(this, NotificationActionsReceiver.class);
            prevIntent.putExtra(Constants.PLAYER_ACTION_FROM_NOTIFICATION, Constants.PLAYER_PREV);
            PendingIntent pendingPrevIntent = PendingIntent.getBroadcast(this, 2, prevIntent, PendingIntent.FLAG_UPDATE_CURRENT);
            remoteView.setOnClickPendingIntent(R.id.btnPrev, pendingPrevIntent);

            Intent stopIntent = new Intent(this, NotificationActionsReceiver.class);
            stopIntent.putExtra(Constants.PLAYER_ACTION_FROM_NOTIFICATION, Constants.PLAYER_STOP);
            PendingIntent pendingStopIntent = PendingIntent.getBroadcast(this, 3, stopIntent, PendingIntent.FLAG_UPDATE_CURRENT);
            remoteView.setOnClickPendingIntent(R.id.btnClose, pendingStopIntent);

            if(PlayerStateHandler.getInstance().isPlaying()){
                remoteView.setImageViewResource(R.id.btnPlayPause, R.drawable.btn_pause);
            }else{
                remoteView.setImageViewResource(R.id.btnPlayPause, R.drawable.btn_play);
            }

            try {
                Bitmap radioImage = Picasso.with(getApplicationContext()).load(currentRadio.getImage()).get();
                remoteView.setImageViewBitmap(R.id.radio_image, radioImage);
            } catch (IOException e) {
                e.printStackTrace();
            }

            notification.bigContentView = remoteView;
        }

        mNotifyMgr.notify(classID, notification);

        startForeground(classID, notification);
    }
}
