package com.hazuu.dailyacoustic.helper;

import android.app.Application;
import android.content.SharedPreferences;

/**
 * Created by donnguyen on 6/21/14.
 */
public class Preference {
    private static SharedPreferences preference;

    public static void initialize(Application application) {
        preference = application.getSharedPreferences(Constants.PREFERENCE_NAME,
                0);
    }

    public static void saveLastUpdateTime(String lastUpdateTime) {
        SharedPreferences.Editor editor = preference.edit();
        editor.putString(Constants.PREFERENCE_LAST_UPDATE_TIME, lastUpdateTime);
        editor.commit();
    }

    public static String getLastUpdateTime() {
        return preference.getString(
                Constants.PREFERENCE_LAST_UPDATE_TIME, "");
    }
}
