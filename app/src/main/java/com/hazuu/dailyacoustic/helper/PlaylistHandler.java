package com.hazuu.dailyacoustic.helper;

import com.hazuu.dailyacoustic.models.Radio;

import java.util.ArrayList;

/**
 * Created by donnguyen on 6/26/14.
 */
public class PlaylistHandler {
    private static PlaylistHandler playlistHandler;
    private ArrayList<Radio> radios;
    private Radio currentRadio;

    public static PlaylistHandler getInstance(){
        if(playlistHandler == null){
            playlistHandler = new PlaylistHandler();
        }

        return playlistHandler;
    }

    public ArrayList<Radio> getRadios() {
        return radios;
    }

    public void setRadios(ArrayList<Radio> radios) {
        this.radios = radios;
    }

    public Radio getCurrentRadio() {
        return currentRadio;
    }

    public void setCurrentRadio(Radio currentRadio) {
        this.currentRadio = currentRadio;
    }

    public Radio getNextRadio(){
        int currentIndex = radios.indexOf(currentRadio);
        if(currentIndex < 0){
            return null;
        }else if(currentIndex + 1 == radios.size()){
            currentRadio = radios.get(0);
            return radios.get(0);
        }else {
            currentRadio = radios.get(currentIndex + 1);
            return radios.get(currentIndex + 1);
        }
    }

    public Radio getPrevRadio(){
        int currentIndex = radios.indexOf(currentRadio);
        if(currentIndex < 0){
            return null;
        }else if(currentIndex == 0){
            currentRadio = radios.get(radios.size() - 1);
            return radios.get(radios.size() - 1);
        }else {
            currentRadio = radios.get(currentIndex - 1);
            return radios.get(currentIndex - 1);
        }
    }
}
