package com.hazuu.dailyacoustic.helper;

/**
 * Created by ea on 6/27/14.
 */
public class PlayerStateHandler {
    private static PlayerStateHandler playerStateHandler;
    private int state = Constants.PLAYER_STATE_IS_STOP;

    public static PlayerStateHandler getInstance(){
        if(playerStateHandler == null){
            playerStateHandler = new PlayerStateHandler();
        }
        return playerStateHandler;
    }

    public int getState() {
        return state;
    }

    public void setState(int state) {
        this.state = state;
    }

    public boolean isStop(){
        return state == Constants.PLAYER_STATE_IS_STOP;
    }

    public boolean isPlaying(){
        return state == Constants.PLAYER_STATE_IS_PLAYING;
    }

    public boolean isPause(){
        return state == Constants.PLAYER_STATE_IS_PAUSE;
    }
}
