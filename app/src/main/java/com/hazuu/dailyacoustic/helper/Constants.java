package com.hazuu.dailyacoustic.helper;

/**
 * Created by donnguyen on 6/20/14.
 */
public class Constants {
    public static final String API_URL = "http://acoustic.hazuu.com/";
    public static final String PREFERENCE_NAME = "DailyAcoustic";
    public static final String PREFERENCE_LAST_UPDATE_TIME = "preference_last_update_time";
    public static final long ONE_HOUR_IN_MILLIS = 3600000;
    public static final String CURRENT_RADIO_ID = "current_radio_id";
    public static final String PLAYER_ACTION_FROM_NOTIFICATION = "player_action_from_notification";
    public static final int PLAYER_RESUME_OR_PLAY = 1;
    public static final int PLAYER_NEXT = 2;
    public static final int PLAYER_PREV = 3;
    public static final int PLAYER_STOP = 4;

    public static final int PLAYER_STATE_IS_STOP = 1;
    public static final int PLAYER_STATE_IS_PLAYING = 2;
    public static final int PLAYER_STATE_IS_PAUSE = 3;
}
