package com.hazuu.dailyacoustic.receivers;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;

import com.hazuu.dailyacoustic.events.RadioPlayerActionEvents;
import com.hazuu.dailyacoustic.helper.Constants;
import com.hazuu.dailyacoustic.helper.PlaylistHandler;
import com.hazuu.dailyacoustic.models.Radio;

import de.greenrobot.event.EventBus;

/**
 * Created by donnguyen on 6/25/14.
 */
public class NotificationActionsReceiver extends BroadcastReceiver{
    @Override
    public void onReceive(Context context, Intent intent) {
        int action = intent.getExtras().getInt(Constants.PLAYER_ACTION_FROM_NOTIFICATION);
        switch (action){
            case Constants.PLAYER_RESUME_OR_PLAY:
                EventBus.getDefault().post(new RadioPlayerActionEvents.PauseOrResumeEvent());
                break;
            case Constants.PLAYER_NEXT:
                Radio nextRadio = PlaylistHandler.getInstance().getNextRadio();
                EventBus.getDefault().post(new RadioPlayerActionEvents.NextEvent(nextRadio));
                break;
            case Constants.PLAYER_PREV:
                Radio prevRadio = PlaylistHandler.getInstance().getPrevRadio();
                EventBus.getDefault().post(new RadioPlayerActionEvents.PrevEvent(prevRadio));
                break;
            case Constants.PLAYER_STOP:
                EventBus.getDefault().post(new RadioPlayerActionEvents.StopEvent());
                break;
            default:
                break;

        }
    }
}
