package com.hazuu.dailyacoustic.ui.fragments;

import android.app.ActionBar;
import android.app.Fragment;
import android.content.Intent;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.SeekBar;
import android.widget.TextView;

import com.google.android.gms.ads.AdRequest;
import com.google.android.gms.ads.AdView;
import com.hazuu.dailyacoustic.R;
import com.hazuu.dailyacoustic.events.PlayerErrorEvent;
import com.hazuu.dailyacoustic.events.PlayerStateUpdateEvent;
import com.hazuu.dailyacoustic.events.RadioPlayerActionEvents;
import com.hazuu.dailyacoustic.events.UpdateBufferingEvent;
import com.hazuu.dailyacoustic.events.UpdatedCurrentRadioEvent;
import com.hazuu.dailyacoustic.helper.PlayerStateHandler;
import com.hazuu.dailyacoustic.helper.PlaylistHandler;
import com.hazuu.dailyacoustic.helper.Utils;
import com.hazuu.dailyacoustic.models.Radio;
import com.hazuu.dailyacoustic.services.MediaPlayerService;
import com.squareup.picasso.Picasso;

import butterknife.ButterKnife;
import butterknife.InjectView;
import butterknife.OnClick;
import de.greenrobot.event.EventBus;

/**
 * Created by donnguyen on 6/22/14.
 */
public class PlayerFragment extends Fragment implements SeekBar.OnSeekBarChangeListener{
    @InjectView(R.id.radio_image)
    ImageView radioImageView;
    @InjectView(R.id.seek_bar_progress)
    SeekBar seekBar;
    @InjectView(R.id.current_time)
    TextView currentTimeTextView;
    @InjectView(R.id.total_time)
    TextView totalTimeTextView;
    @InjectView(R.id.btnPlay)
    ImageButton playImageButton;
    @InjectView(R.id.btnNext)
    ImageButton nextImageButton;
    @InjectView(R.id.btnPrev)
    ImageButton prevImageButton;
    @InjectView(R.id.adView)
    AdView adView;

    private Radio radio;
    private int totalDuration;
    ActionBar actionBar;

    public PlayerFragment() {
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setHasOptionsMenu(true);

        actionBar = getActivity().getActionBar();
        actionBar.setDisplayHomeAsUpEnabled(true);
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        View rootView = inflater.inflate(R.layout.fragment_player, container, false);
        ButterKnife.inject(this, rootView);
        return rootView;
    }

    @Override
    public void onViewCreated(View view, Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        initializeView();
        initializeAds();
    }
    private void initializeAds() {
        AdRequest adRequest = new AdRequest.Builder().build();
        adView.loadAd(adRequest);
    }

    @Override
    public void onCreateOptionsMenu(Menu menu, MenuInflater inflater) {
        super.onCreateOptionsMenu(menu, inflater);
        if (menu != null) {
            menu.findItem(R.id.action_refresh).setVisible(false);
        }
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case android.R.id.home:
                getActivity().onBackPressed();
                return true;
            default:
                return super.onOptionsItemSelected(item);
        }
    }

    @Override
    public void onPause() {
        super.onPause();
        EventBus.getDefault().unregister(this);
    }

    @Override
    public void onResume() {
        super.onResume();
        EventBus.getDefault().registerSticky(this);
    }

    @Override
    public void onDestroyView() {
        super.onDestroyView();
        ButterKnife.reset(this);
    }

    @Override
    public void onProgressChanged(SeekBar seekBar, int i, boolean b) {

    }

    @Override
    public void onStartTrackingTouch(SeekBar seekBar) {

    }

    @Override
    public void onStopTrackingTouch(SeekBar seekBar) {
        int currentPosition = Utils.progressToTimer(seekBar.getProgress(), totalDuration);

        EventBus.getDefault().post(new RadioPlayerActionEvents.SeekToEvent(currentPosition));
    }

    @OnClick(R.id.btnPlay)
    public void onClickPlayOrPause(){
        Intent intent = new Intent(getActivity().getApplicationContext(),
                MediaPlayerService.class);
        if(getActivity().startService(intent) != null){
            if (PlayerStateHandler.getInstance().isStop()) {
                playImageButton.setImageDrawable(getActivity().getResources().getDrawable(R.drawable.btn_pause));
                EventBus.getDefault().postSticky(new RadioPlayerActionEvents.PlayEvent(radio));
            } else if(PlayerStateHandler.getInstance().isPlaying()){
                playImageButton.setImageDrawable(getActivity().getResources().getDrawable(R.drawable.btn_play));
                EventBus.getDefault().post(new RadioPlayerActionEvents.PauseOrResumeEvent());
            }else{
                playImageButton.setImageDrawable(getActivity().getResources().getDrawable(R.drawable.btn_pause));
                EventBus.getDefault().post(new RadioPlayerActionEvents.PauseOrResumeEvent());
            }

        }
    }

    @OnClick(R.id.btnNext)
    public void onClickNext(){
        radio = PlaylistHandler.getInstance().getNextRadio();
        EventBus.getDefault().post(new RadioPlayerActionEvents.NextEvent(radio));
    }

    @OnClick(R.id.btnPrev)
    public void onClickPrev(){
        radio = PlaylistHandler.getInstance().getPrevRadio();
        EventBus.getDefault().post(new RadioPlayerActionEvents.PrevEvent(radio));
    }

    public void onEventMainThread(RadioPlayerActionEvents.NextEvent nextEvent) {
        radio = nextEvent.getRadio();
        refreshView();
    }

    public void onEventMainThread(RadioPlayerActionEvents.PrevEvent prevEvent) {
        radio = prevEvent.getRadio();
        refreshView();
    }

    public void onEventMainThread(PlayerStateUpdateEvent playerStateUpdateEvent){
        updateView(playerStateUpdateEvent);
    }

    public void onEventMainThread(UpdateBufferingEvent updateBuffering){
        seekBar.setSecondaryProgress(updateBuffering.getPercent());
    }

    public void onEventMainThread(PlayerErrorEvent playerErrorEvent){
        resetView();
    }

    private void resetView() {
        playImageButton.setImageDrawable(getActivity().getResources().getDrawable(R.drawable.btn_play));
        currentTimeTextView.setText(getActivity().getString(R.string.empty_time));
        totalTimeTextView.setText(getActivity().getString(R.string.empty_time));
        seekBar.setProgress(0);
        seekBar.setSecondaryProgress(0);
    }

    public void onEventMainThread(RadioPlayerActionEvents.StopEvent stopEvent) {
        resetView();
    }

    public void initializeView(){
        UpdatedCurrentRadioEvent updatedCurrentRadioEvent = EventBus.getDefault().getStickyEvent(UpdatedCurrentRadioEvent.class);
        radio = updatedCurrentRadioEvent.getCurrentRadio();

        Intent intent = new Intent(getActivity().getApplicationContext(),
                MediaPlayerService.class);
        getActivity().startService(intent);

        if(radio != null){
            actionBar.setTitle(radio.getTitle());
            Picasso.with(getActivity())
                    .load(radio.getImage())
                    .placeholder(R.drawable.bg_radio_holder)
                    .error(R.drawable.bg_radio_holder)
                    .into(radioImageView);

            PlayerStateUpdateEvent playerStateUpdateEvent = EventBus.getDefault().getStickyEvent(PlayerStateUpdateEvent.class);
            if(playerStateUpdateEvent == null || radio != playerStateUpdateEvent.getCurrentRadio()){ //Start playing new radio
                EventBus.getDefault().postSticky(new RadioPlayerActionEvents.PlayEvent(radio));
                playImageButton.setImageDrawable(getActivity().getResources().getDrawable(R.drawable.btn_pause));
            }else{
                updateView(playerStateUpdateEvent);
            }
        }

        seekBar.setOnSeekBarChangeListener(this);
    }

    private void refreshView() {
        actionBar.setTitle(radio.getTitle());
        Picasso.with(getActivity())
                .load(radio.getImage())
                .placeholder(R.drawable.bg_radio_holder)
                .error(R.drawable.bg_radio_holder)
                .into(radioImageView);
    }

    private void updateView(PlayerStateUpdateEvent playerStateUpdateEvent){
        int currentDuration = playerStateUpdateEvent.getCurrentDuration();
        totalDuration = playerStateUpdateEvent.getTotalDuration();

        seekBar.setProgress(Utils.getProgressPercentage(currentDuration, totalDuration));
        currentTimeTextView.setText(Utils.milliSecondsToTimer(currentDuration));
        totalTimeTextView.setText(Utils.milliSecondsToTimer(totalDuration));

        if(PlayerStateHandler.getInstance().isPlaying()){
            playImageButton.setImageDrawable(getActivity().getResources().getDrawable(R.drawable.btn_pause));
        }else{
            playImageButton.setImageDrawable(getActivity().getResources().getDrawable(R.drawable.btn_play));
        }
    }
}
