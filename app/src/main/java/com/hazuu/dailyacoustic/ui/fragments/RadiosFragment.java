package com.hazuu.dailyacoustic.ui.fragments;

import android.app.ActionBar;
import android.app.Fragment;
import android.content.Intent;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ListView;
import android.widget.TextView;

import com.google.android.gms.ads.AdRequest;
import com.google.android.gms.ads.AdView;
import com.hazuu.dailyacoustic.R;
import com.hazuu.dailyacoustic.events.ClickOnRadioEvent;
import com.hazuu.dailyacoustic.events.PlayerStateUpdateEvent;
import com.hazuu.dailyacoustic.events.RadioPlayerActionEvents;
import com.hazuu.dailyacoustic.events.UpdatePlaylistEvent;
import com.hazuu.dailyacoustic.events.UpdatedCurrentRadioEvent;
import com.hazuu.dailyacoustic.events.UpdatedRadiosEvent;
import com.hazuu.dailyacoustic.helper.PlayerStateHandler;
import com.hazuu.dailyacoustic.models.Radio;
import com.hazuu.dailyacoustic.models.RadioRepository;
import com.hazuu.dailyacoustic.services.MediaPlayerService;
import com.hazuu.dailyacoustic.ui.adapters.RadiosAdapter;
import com.squareup.picasso.Picasso;

import java.util.ArrayList;

import butterknife.ButterKnife;
import butterknife.InjectView;
import butterknife.OnClick;
import de.greenrobot.event.EventBus;

/**
 * Created by donnguyen on 6/21/14.
 */
public class RadiosFragment extends Fragment{
    private RadiosAdapter radiosAdapter;
    private ArrayList<Radio> radios;
    ActionBar actionBar;
    private Radio currentRadio;

    @InjectView(R.id.radios_list_view)
    ListView radiosListView;
    @InjectView(R.id.small_control_layout)
    LinearLayout smallControlLayout;
    @InjectView(R.id.btnPlaySmallControl)
    ImageView btnPlayPause;
    @InjectView(R.id.radio_image_small_control)
    ImageView radioImage;
    @InjectView(R.id.radio_title_on_small_control_bar)
    TextView radioTitle;
    @InjectView(R.id.adView)
    AdView adView;

    public RadiosFragment() {
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setHasOptionsMenu(true);

        actionBar = getActivity().getActionBar();

        UpdatedCurrentRadioEvent updatedCurrentRadioEvent = EventBus.getDefault().getStickyEvent(UpdatedCurrentRadioEvent.class);
        if(updatedCurrentRadioEvent != null){
            currentRadio = updatedCurrentRadioEvent.getCurrentRadio();
        }
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        View rootView = inflater.inflate(R.layout.fragment_radios, container, false);
        ButterKnife.inject(this, rootView);

        return rootView;
    }

    @Override
    public void onViewCreated(View view, Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);

        if(currentRadio != null){
            initializeSmallControlBar();
        }

        initializeRadiosListView();
        initializeAds();
    }

    private void initializeAds() {
        AdRequest adRequest = new AdRequest.Builder().build();
        adView.loadAd(adRequest);
    }

    @Override
    public void onPause() {
        super.onPause();
        EventBus.getDefault().unregister(this);
    }

    @Override
    public void onResume() {
        super.onResume();
        actionBar.setDisplayHomeAsUpEnabled(false);
        actionBar.setTitle(getActivity().getResources().getString(R.string.app_name));
        EventBus.getDefault().registerSticky(this);
    }

    @Override
    public void onDestroyView() {
        super.onDestroyView();
        ButterKnife.reset(this);
    }

    @OnClick(R.id.btnPlaySmallControl)
    public void onClickPlayOrPause(){
        Intent intent = new Intent(getActivity().getApplicationContext(),
                MediaPlayerService.class);
        if(getActivity().startService(intent) != null){
            if (PlayerStateHandler.getInstance().isStop()) {
                btnPlayPause.setImageDrawable(getActivity().getResources().getDrawable(R.drawable.btn_pause));
                EventBus.getDefault().postSticky(new RadioPlayerActionEvents.PlayEvent(currentRadio));
            } else if(PlayerStateHandler.getInstance().isPlaying()){
                btnPlayPause.setImageDrawable(getActivity().getResources().getDrawable(R.drawable.btn_play));
                EventBus.getDefault().post(new RadioPlayerActionEvents.PauseOrResumeEvent());
            }else{
                btnPlayPause.setImageDrawable(getActivity().getResources().getDrawable(R.drawable.btn_pause));
                EventBus.getDefault().post(new RadioPlayerActionEvents.PauseOrResumeEvent());
            }

        }
    }

    @OnClick(R.id.small_control_layout)
    public void onClickSmallControlBar(){
        EventBus.getDefault().post(new ClickOnRadioEvent(currentRadio));
    }

    public void onEvent(UpdatedRadiosEvent updatedRadiosEvent){
        if(updatedRadiosEvent.getRadios() != null){
            radios.addAll(0, updatedRadiosEvent.getRadios());
            radiosAdapter.notifyDataSetChanged();

            EventBus.getDefault().postSticky(new UpdatePlaylistEvent(radios));
        }
    }

    public void onEvent(UpdatedCurrentRadioEvent updatedCurrentRadioEvent){
        currentRadio = updatedCurrentRadioEvent.getCurrentRadio();
        initializeSmallControlBar();
    }

    public void onEventMainThread(RadioPlayerActionEvents.StopEvent stopEvent) {
        btnPlayPause.setImageDrawable(getActivity().getResources().getDrawable(R.drawable.btn_play));
    }

    public void onEventMainThread(PlayerStateUpdateEvent playerStateUpdateEvent){
        updateView(playerStateUpdateEvent);
    }

    private void initializeRadiosListView() {
        radios = RadioRepository.getAllRadios(getActivity());
        radiosAdapter = new RadiosAdapter(getActivity(), radios);
        radiosListView.setAdapter(radiosAdapter);

        EventBus.getDefault().postSticky(new UpdatePlaylistEvent(radios));
    }

    private void initializeSmallControlBar() {
        smallControlLayout.setVisibility(View.VISIBLE);

        Picasso.with(getActivity())
                .load(currentRadio.getImage())
                .placeholder(R.drawable.bg_radio_holder)
                .error(R.drawable.bg_radio_holder)
                .into(radioImage);

        radioTitle.setText(currentRadio.getTitle());

        if(PlayerStateHandler.getInstance().isPlaying()){
            btnPlayPause.setImageDrawable(getActivity().getResources().getDrawable(R.drawable.btn_pause));
        }else{
            btnPlayPause.setImageDrawable(getActivity().getResources().getDrawable(R.drawable.btn_play));
        }
    }

    private void updateView(PlayerStateUpdateEvent playerStateUpdateEvent){
        if(PlayerStateHandler.getInstance().isPlaying()){
            btnPlayPause.setImageDrawable(getActivity().getResources().getDrawable(R.drawable.btn_pause));
        }else{
            btnPlayPause.setImageDrawable(getActivity().getResources().getDrawable(R.drawable.btn_play));
        }
    }
}
