package com.hazuu.dailyacoustic.ui.adapters;

import android.content.Context;
import android.text.Spannable;
import android.text.SpannableString;
import android.text.style.BackgroundColorSpan;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.TextView;

import com.hazuu.dailyacoustic.R;
import com.hazuu.dailyacoustic.events.ClickOnRadioEvent;
import com.hazuu.dailyacoustic.models.Radio;
import com.squareup.picasso.Picasso;

import java.util.ArrayList;

import de.greenrobot.event.EventBus;

/**
 * Created by donnguyen on 6/21/14.
 */
public class RadiosAdapter extends BaseAdapter{
    private Context context;

    private ArrayList<Radio> radios;
    private LayoutInflater inflater;

    public RadiosAdapter(Context context, ArrayList<Radio> radios) {
        this.context = context;

        this.radios = radios;
        inflater = (LayoutInflater) context
                .getSystemService(Context.LAYOUT_INFLATER_SERVICE);
    }

    @Override
    public int getCount() {
        return radios.size();
    }

    @Override
    public Object getItem(int position) {
        return radios.get(position);
    }

    @Override
    public long getItemId(int position) {
        return 0;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        ViewHolder holder = null;
        final Radio item = radios.get(position);
        if (convertView == null) {
            holder = new ViewHolder();
            convertView = inflater.inflate(R.layout.radio_list_item, null);

            holder.radioTitle = (TextView) convertView
                    .findViewById(R.id.radio_title);
            holder.radioImage = (ImageView) convertView
                    .findViewById(R.id.radio_image);

            convertView.setTag(holder);
        } else {
            holder = (ViewHolder) convertView.getTag();
        }

        Spannable radioTitleStyled = new SpannableString(item.getTitle());
        int backgroundColor = context.getResources().getColor(R.color.radio_name_color);
        radioTitleStyled.setSpan(new BackgroundColorSpan(backgroundColor), 0, item.getTitle().length(), Spannable.SPAN_EXCLUSIVE_EXCLUSIVE);
        holder.radioTitle.setText(radioTitleStyled);

        Picasso.with(context)
                .load(item.getImage())
                .resize(320, 180)
                .centerCrop()
                .placeholder(R.drawable.bg_radio_holder)
                .error(R.drawable.bg_radio_holder)
                .into(holder.radioImage);

        holder.radioImage.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                EventBus.getDefault().post(new ClickOnRadioEvent(item));
            }
        });

        return convertView;
    }

    class ViewHolder {
        TextView radioTitle;
        ImageView radioImage;
    }
}
