package com.hazuu.dailyacoustic.ui;

import android.app.Activity;
import android.content.Intent;
import android.os.Build;
import android.os.Bundle;
import android.text.TextUtils;
import android.view.Menu;
import android.view.MenuItem;

import com.hazuu.dailyacoustic.R;
import com.hazuu.dailyacoustic.api.APIConnector;
import com.hazuu.dailyacoustic.events.ClickOnRadioEvent;
import com.hazuu.dailyacoustic.events.PlayerErrorEvent;
import com.hazuu.dailyacoustic.events.UpdatePlaylistEvent;
import com.hazuu.dailyacoustic.events.UpdatedCurrentRadioEvent;
import com.hazuu.dailyacoustic.events.UpdatedRadiosEvent;
import com.hazuu.dailyacoustic.helper.PlayerStateHandler;
import com.hazuu.dailyacoustic.helper.PlaylistHandler;
import com.hazuu.dailyacoustic.helper.Preference;
import com.hazuu.dailyacoustic.helper.Utils;
import com.hazuu.dailyacoustic.models.Radio;
import com.hazuu.dailyacoustic.models.RadioRepository;
import com.hazuu.dailyacoustic.models.response.RadioResponse;
import com.hazuu.dailyacoustic.services.MediaPlayerService;
import com.hazuu.dailyacoustic.ui.fragments.PlayerFragment;
import com.hazuu.dailyacoustic.ui.fragments.RadiosFragment;

import java.util.ArrayList;

import de.greenrobot.event.EventBus;
import de.keyboardsurfer.android.widget.crouton.Crouton;
import de.keyboardsurfer.android.widget.crouton.Style;
import retrofit.Callback;
import retrofit.RetrofitError;
import retrofit.client.Response;


public class MainActivity extends Activity{
    ArrayList<Radio> radios;
    private Menu optionsMenu;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        if (savedInstanceState == null) {
            getFragmentManager().beginTransaction()
                    .add(R.id.container, new RadiosFragment())
                    .commit();
        }

        updateRadiosIfNeed(false);
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        this.optionsMenu = menu;
        getMenuInflater().inflate(R.menu.main, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        int id = item.getItemId();
        switch (id) {
            case R.id.action_refresh:
                updateRadiosIfNeed(true);
                break;
        }
        return super.onOptionsItemSelected(item);
    }

    public void setRefreshActionButtonState(final boolean refreshing) {
        if (optionsMenu != null) {
            final MenuItem refreshItem = optionsMenu
                    .findItem(R.id.action_refresh);
            if (refreshItem != null) {
                if (refreshing) {
                    refreshItem.setActionView(R.layout.actionbar_indeterminate_progress);
                } else {
                    refreshItem.setActionView(null);
                }
            }
        }
    }

    @Override
    public void onPause() {
        super.onPause();
        EventBus.getDefault().unregister(this);
    }

    @Override
    public void onResume() {
        super.onResume();
        EventBus.getDefault().registerSticky(this);
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        Crouton.cancelAllCroutons();
        if(Build.VERSION.SDK_INT < 16 && !PlayerStateHandler.getInstance().isPlaying()){
            Intent intent = new Intent(getApplicationContext(),
                    MediaPlayerService.class);
            stopService(intent);
        }
    }

    public void onEvent(ClickOnRadioEvent clickOnRadioEvent) {
        PlaylistHandler.getInstance().setCurrentRadio(clickOnRadioEvent.getRadio());
        EventBus.getDefault().postSticky(new UpdatedCurrentRadioEvent(clickOnRadioEvent.getRadio()));
        showPlayerFragment();
    }

    public void onEvent(UpdatePlaylistEvent updatePlaylistEvent){
        PlaylistHandler.getInstance().setRadios(updatePlaylistEvent.getRadios());
    }

    public void onEventMainThread(PlayerErrorEvent playerErrorEvent){
        Crouton.makeText(this, getString(R.string.cannot_play_radio_right_now), Style.ALERT).show();
    }

    private void updateRadiosIfNeed(boolean showCrouton){
        String lastUpdateTime = Preference.getLastUpdateTime();
        if(TextUtils.isEmpty(lastUpdateTime) || Utils.isMoreThan1Hour(lastUpdateTime)){
            getRadios(lastUpdateTime);
        }else if(showCrouton){
            Crouton.makeText(this, getString(R.string.no_more_radios), Style.ALERT).show();
        }
    }

    private void getRadios(String lastUpdateTime){
        setRefreshActionButtonState(true);

        APIConnector.acoustic.getAllRadios(lastUpdateTime, new Callback<RadioResponse>() {
            @Override
            public void success(RadioResponse radioResponse, Response response) {
                radios = radioResponse.getRadios();
                RadioRepository.insertOrUpdateInTx(MainActivity.this, radios);

                //Post data to update fragment
                EventBus.getDefault().post(updatedRadiosEvent());

                //Remember to save last updated time
                String currentTime = Utils.getCurrentTime();
                Preference.saveLastUpdateTime(currentTime);

                setRefreshActionButtonState(false);
            }

            @Override
            public void failure(RetrofitError retrofitError) {
                setRefreshActionButtonState(false);
            }
        });
    }

    public UpdatedRadiosEvent updatedRadiosEvent() {
        return new UpdatedRadiosEvent(radios);
    }

    public void showPlayerFragment(){
        getFragmentManager().beginTransaction()
                .replace(R.id.container, new PlayerFragment())
                .addToBackStack(null)
                .commit();
    }
}
