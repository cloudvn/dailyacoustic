package com.hazuu.dailyacoustic.models;

import android.content.Context;

import com.hazuu.dailyacoustic.AcousticApplication;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by donnguyen on 6/21/14.
 */
public class RadioRepository {
    public static void insertOrUpdateInTx(Context context, ArrayList<Radio> radios){
        getRadioDao(context).insertOrReplaceInTx(radios);
    }

    public static void insertOrUpdate(Context context, Radio radio) {
        getRadioDao(context).insertOrReplace(radio);
    }

    public static void clearRadios(Context context) {
        getRadioDao(context).deleteAll();
    }

    public static void deleteRadioById(Context context, long id) {
        getRadioDao(context).delete(getRadioById(context, id));
    }

    public static Radio getRadioById(Context context, long id) {
        return getRadioDao(context).load(id);
    }

    public static ArrayList<Radio> getAllRadios(Context context) {
        List<Radio> radios = getRadioDao(context).queryBuilder()
                .orderDesc(RadioDao.Properties.Id).list();
        return new ArrayList<Radio>(radios);
    }
    
    private static RadioDao getRadioDao(Context c) {
        return ((AcousticApplication) c.getApplicationContext()).getDaoSession().getRadioDao();
    }
}
