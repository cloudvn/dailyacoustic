package com.hazuu.dailyacoustic.models.response;

import com.hazuu.dailyacoustic.models.Radio;

import java.util.ArrayList;

/**
 * Created by donnguyen on 6/20/14.
 */
public class RadioResponse {
    ArrayList<Radio> radios;

    public ArrayList<Radio> getRadios() {
        return radios;
    }

    public void setRadios(ArrayList<Radio> radios) {
        this.radios = radios;
    }
}
