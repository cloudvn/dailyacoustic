package com.hazuu.dailyacoustic.events;

import com.hazuu.dailyacoustic.models.Radio;

/**
 * Created by donnguyen on 6/22/14.
 */
public class ClickOnRadioEvent {
    private Radio radio;

    public ClickOnRadioEvent(Radio radio) {
        this.radio = radio;
    }

    public Radio getRadio() {
        return radio;
    }
}
