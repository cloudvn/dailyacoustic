package com.hazuu.dailyacoustic.events;

import com.hazuu.dailyacoustic.models.Radio;

/**
 * Created by donnguyen on 6/26/14.
 */
public class UpdatedCurrentRadioEvent {
    private Radio currentRadio;

    public UpdatedCurrentRadioEvent(Radio currentRadio) {

        this.currentRadio = currentRadio;
    }

    public Radio getCurrentRadio() {
        return currentRadio;
    }
}
