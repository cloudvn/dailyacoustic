package com.hazuu.dailyacoustic.events;

import com.hazuu.dailyacoustic.models.Radio;

import java.util.ArrayList;

/**
 * Created by donnguyen on 6/26/14.
 */
public class UpdatePlaylistEvent {
    private ArrayList<Radio> radios;

    public UpdatePlaylistEvent(ArrayList<Radio> radios) {
        this.radios = radios;
    }

    public ArrayList<Radio> getRadios() {
        return radios;
    }
}
