package com.hazuu.dailyacoustic.events;

import com.hazuu.dailyacoustic.models.Radio;

/**
 * Created by donnguyen on 6/24/14.
 */
public class PlayerStateUpdateEvent {
    private int currentDuration;
    private int totalDuration;
    private boolean isPlaying;
    private Radio currentRadio;

    public PlayerStateUpdateEvent(int currentDuration, int totalDuration, boolean isPlaying, Radio currentRadio) {
        this.currentDuration = currentDuration;
        this.totalDuration = totalDuration;
        this.isPlaying = isPlaying;
        this.currentRadio = currentRadio;
    }

    public int getCurrentDuration() {
        return currentDuration;
    }

    public int getTotalDuration() {
        return totalDuration;
    }

    public boolean isPlaying() {
        return isPlaying;
    }

    public Radio getCurrentRadio() {
        return currentRadio;
    }
}
