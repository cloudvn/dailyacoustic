package com.hazuu.dailyacoustic.events;

/**
 * Created by donnguyen on 6/22/14.
 */
public class UpdateBufferingEvent {
    private int percent;

    public UpdateBufferingEvent(int percent) {
        this.percent = percent;
    }

    public int getPercent() {
        return percent;
    }
}
