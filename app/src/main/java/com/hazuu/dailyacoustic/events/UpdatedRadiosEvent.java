package com.hazuu.dailyacoustic.events;

import com.hazuu.dailyacoustic.models.Radio;

import java.util.ArrayList;

/**
 * Created by donnguyen on 6/21/14.
 */
public class UpdatedRadiosEvent {
    private ArrayList<Radio> radios;

    public UpdatedRadiosEvent(ArrayList<Radio> radios) {
        this.radios = radios;
    }

    public ArrayList<Radio> getRadios() {
        return radios;
    }
}
