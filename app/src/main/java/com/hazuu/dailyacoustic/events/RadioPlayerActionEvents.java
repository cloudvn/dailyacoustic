package com.hazuu.dailyacoustic.events;

import com.hazuu.dailyacoustic.models.Radio;

/**
 * Created by donnguyen on 6/24/14.
 */
public class RadioPlayerActionEvents {
    public static class PlayEvent{
        private Radio radio;

        public PlayEvent(Radio radio) {
            this.radio = radio;
        }

        public Radio getRadio() {
            return radio;
        }
    }

    public static class PauseOrResumeEvent {
        public PauseOrResumeEvent() {
        }
    }

    public static class NextEvent extends PlayEvent{
        public NextEvent(Radio radio) {
            super(radio);
        }
    }

    public static class PrevEvent extends PlayEvent{
        public PrevEvent(Radio radio) {
            super(radio);
        }
    }

    public static class SeekToEvent{
        private int position;

        public SeekToEvent(int position) {
            this.position = position;
        }

        public int getPosition() {
            return position;
        }
    }

    public static class StopEvent {
        public StopEvent() {
        }
    }
}
