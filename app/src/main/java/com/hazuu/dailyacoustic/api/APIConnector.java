package com.hazuu.dailyacoustic.api;

import com.hazuu.dailyacoustic.helper.Constants;
import com.hazuu.dailyacoustic.models.response.RadioResponse;

import retrofit.Callback;
import retrofit.RestAdapter;
import retrofit.client.OkClient;
import retrofit.http.GET;
import retrofit.http.Query;

/**
 * Created by donnguyen on 6/20/14.
 */
public class APIConnector {
    private static RestAdapter restAdapter;
    public static Acoustic acoustic;

    public static void initialize() {
        restAdapter = new RestAdapter.Builder()
                .setClient(new OkClient()).setEndpoint(Constants.API_URL)
                .build();

        // Create an instance of our GameHawk API interface.
        acoustic = restAdapter.create(Acoustic.class);
    }

    public interface Acoustic {
        @GET("/radios.json")
        void getAllRadios(@Query("time") String time, Callback<RadioResponse> callback);
    }
}
