package com.hazuu.dailyacoustic;

import android.app.Application;
import android.database.sqlite.SQLiteDatabase;

import com.hazuu.dailyacoustic.api.APIConnector;
import com.hazuu.dailyacoustic.helper.Preference;
import com.hazuu.dailyacoustic.models.DaoMaster;
import com.hazuu.dailyacoustic.models.DaoSession;

/**
 * Created by donnguyen on 6/20/14.
 */
public class AcousticApplication extends Application {
    public DaoSession daoSession;

    @Override
    public void onCreate() {
        super.onCreate();
        APIConnector.initialize();
        Preference.initialize(this);
        setupDatabase();
    }

    private void setupDatabase() {
        DaoMaster.DevOpenHelper helper = new DaoMaster.DevOpenHelper(this, "dailyacoustic", null);
        SQLiteDatabase db = helper.getWritableDatabase();
        DaoMaster daoMaster = new DaoMaster(db);
        daoSession = daoMaster.newSession();
    }

    public DaoSession getDaoSession() {
        return daoSession;
    }
}
